#ifndef LEGACYRECTANGLE_H
#define LEGACYRECTANGLE_H
#include <QString>

class LegacyRectangle
{

public:
    LegacyRectangle(int x1, int y1, int x2, int y2);
    void oldDraw();

private:
    int x1_, x2_, y1_, y2_;
};

#endif // LEGACYRECTANGLE_H
