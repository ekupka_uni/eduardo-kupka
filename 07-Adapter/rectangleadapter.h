#ifndef RECTANGLEADAPTER_H
#define RECTANGLEADAPTER_H
#include "rectangle.h"
#include "legacyrectangle.h"

class RectangleAdapter : public Rectangle, private LegacyRectangle
{
public:
    RectangleAdapter(int x, int y, int w, int h);
    void draw();
};

#endif // RECTANGLEADAPTER_H
