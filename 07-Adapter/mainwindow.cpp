#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "rectangle.h"
#include "legacyrectangle.h"
#include "rectangleadapter.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    int x = ui->lineEdit->text().toInt();
    int y = ui->lineEdit_2->text().toInt();
    int w = ui->lineEdit_3->text().toInt();
    int h = ui->lineEdit_4->text().toInt();

    Rectangle *r = new RectangleAdapter(x, y, w, h);


    r->draw();


}
