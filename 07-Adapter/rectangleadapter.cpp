#include "iostream"
#include "rectangleadapter.h"
#include "legacyrectangle.h"
#include "QTextStream"

RectangleAdapter::RectangleAdapter(int x, int y, int w, int h) : LegacyRectangle(x,y,x+w,y+h){

}

void RectangleAdapter::draw(){
    QTextStream(stdout) << "Adapter Draw" << endl;
    oldDraw();
}
