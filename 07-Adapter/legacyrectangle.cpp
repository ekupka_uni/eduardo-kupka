#include <iostream>
#include "legacyrectangle.h"
#include "mainwindow.h"
#include "QTextStream"

LegacyRectangle::LegacyRectangle(int x1, int y1, int x2, int y2){
    x1_ = x1;
    y1_ = y1;
    x2_ = x2;
    y2_ = y2;
}

void LegacyRectangle::oldDraw(){
    QTextStream(stdout) << QString("Legacy Draw: X1=" + QString::number(x1_) + ", X2=" + QString::number(x2_) + ", Y1=" + QString::number(y1_) + ", Y2=" + QString::number(y2_)) << endl;
}
