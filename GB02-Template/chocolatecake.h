#ifndef CHOCOLATECAKE_H
#define CHOCOLATECAKE_H
#include "cake.h"

class ChocolateCake : public Cake
{
    void addMainIngredients(){
        QTextStream(stdout) << "Adicionando Chocolate" << endl;
    }
public:
    ChocolateCake();
};

#endif // CHOCOLATECAKE_H
