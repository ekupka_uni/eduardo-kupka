#ifndef CAKE_H
#define CAKE_H
#include "QTextStream"

class Cake
{
    void prepareBakingContainer(){
        QTextStream(stdout) << "Untando a forma" << endl;
    }
    void addBaseIngredients(){
        QTextStream(stdout) << "Adicionando ingredientes base" << endl;
    }
    void mix(){
        QTextStream(stdout) << "Misturando massa" << endl;
    }
    void bake(){
        QTextStream(stdout) << "Assando bolo" << endl;
    }
    virtual void addMainIngredients() =0;

public:
    Cake();
    void makeTheCake(){
        prepareBakingContainer();
        addBaseIngredients();
        addMainIngredients();
        mix();
        bake();
    }
};

#endif // CAKE_H
