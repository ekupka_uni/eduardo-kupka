#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "cake.h"
#include "chocolatecake.h"
#include "corncake.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    ChocolateCake choco_cake = ChocolateCake();
    choco_cake.makeTheCake();
}
void MainWindow::on_pushButton_2_clicked()
{
    CornCake corn_cake = CornCake();
    corn_cake.makeTheCake();
}
