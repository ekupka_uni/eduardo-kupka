#include "configuration.h"
#include "QTextStream"

Configuration::Configuration()
{}

Configuration* Configuration::instance = 0;


Configuration* Configuration::getInstance(){
    if (instance==0) instance = new Configuration();
    return instance;
}

void Configuration::setConfig(std::string cip, int cport, std::string cname, std::string cpassword){
    ip = cip;
    port = cport;
    name = cname;
    password = cpassword;
}

void Configuration::printInfo(){
    QTextStream(stdout) << QString("IP: " + QString::fromStdString(ip) + "; Porta: " + QString::number(port) + "; Nome: " + QString::fromStdString(name) + "; Senha: " + QString::fromStdString(password) ) << endl;
}
