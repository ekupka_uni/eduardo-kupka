#ifndef CONFIGURATION_H
#define CONFIGURATION_H
#include <iostream>

class Configuration
{
public:
    static Configuration* getInstance();
    void setConfig(std::string cip, int cport, std::string cname, std::string cpassword);
    void printInfo();
    std::string ip;
    int port;
    std::string name;
    std::string password;
private:
    Configuration();
    static Configuration* instance;
};

#endif // CONFIGURATION_H
