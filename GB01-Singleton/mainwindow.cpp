#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "configuration.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    Configuration* c1 = Configuration::getInstance();
    Configuration* c2 = Configuration::getInstance();

    c1->setConfig("192.168.0.1", 3000, "Nome de Usuário", "Senha de Usuário");

    c2->printInfo();
}
