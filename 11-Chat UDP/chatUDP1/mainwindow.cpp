#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    udpSocket = new QUdpSocket(this);

    udpSocket->bind(QHostAddress::LocalHost, 4242);

    connect(udpSocket, SIGNAL(readyRead()), this, SLOT(ler_do_socket()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::ler_do_socket()
{
    while (udpSocket->hasPendingDatagrams()) {
        QByteArray datagrama;

        datagrama.resize(udpSocket->pendingDatagramSize());

        QHostAddress remetente;
        quint16 porta;

        udpSocket->readDatagram(datagrama.data(), datagrama.size(), &remetente, &porta);

        ui->mensagens->append(remetente.toString() + ":" + QString::number(porta) + ": " + QString(datagrama));
    }
}


void MainWindow::on_enviar_clicked()
{
    QByteArray data;
    QHostAddress end = QHostAddress(ui->ip->text());
    data.append(ui->mensagem->text());
    udpSocket->writeDatagram(data, end, 4242);
    ui->mensagem->clear();
}
