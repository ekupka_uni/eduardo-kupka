#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>
#include <QHostAddress>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void ler_do_socket();

private slots:

    void on_enviar_clicked();

private:
    Ui::MainWindow *ui;
    QUdpSocket *udpSocket;
    QHostAddress endereco;
};

#endif // MAINWINDOW_H
