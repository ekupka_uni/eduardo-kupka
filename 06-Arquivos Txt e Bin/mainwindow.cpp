#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "account.h"
#include <QFileDialog>
#include <QTextStream>
#include <algorithm>
#include <iostream>
#include <math.h>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()  //CRIAR CONTA
{
    if(binLoaded){
        if( !accBin.open(QIODevice::ReadWrite) ){
            ui->label_10->setText("Erro");
        } else {
            QString newNumber = ui->lineEdit->text();
            QString newName = ui->lineEdit_2->text();
            QString newBalance = ui->lineEdit_3->text();
            newName.resize(40, ' ');
            account newAccount;
            newAccount.number = newNumber.toInt();
            strcpy(newAccount.name,newName.toLocal8Bit().constData());
            newAccount.balance = newBalance.replace(',','.').toFloat();

            char buffer[sizeof(account)];
            memcpy(buffer, &newAccount, sizeof(account));
            accBin.seek(accBin.size());
            QDataStream out(&accBin);
            out.writeRawData(buffer, sizeof(account));

            ui->label_10->setText("Conta criada");
            ui->lineEdit->clear();
            ui->lineEdit_2->clear();
            ui->lineEdit_3->clear();

            accBin.close();
        }

    } else {
        ui->label_10->setText("Arquivo de contas não encontrado");
    }


    //QStringList splitted = newAccount.split('|');

    //ui->textEdit->append(splitted[0] + "*");
    //ui->textEdit->append(splitted[1] + "*");
    //ui->textEdit->append(splitted[2] + "*");
}

void MainWindow::on_pushButton_2_clicked()  //SELECIONAR ARQUIVO DE CONTAS
{
    QString accountsFile= QFileDialog::getOpenFileName(this, "Selecione o arquivo que contém as contas.", "", "Arquivos binário (*.bin)");
    accBin.setFileName(accountsFile);

    if( !accBin.open(QIODevice::ReadWrite) ){
        ui->label_11->setText("Erro ao abrir arquivo");
    } else {
        binLoaded = true;
        ui->textEdit_2->clear();
        QDataStream in(&accBin);
        int n = accBin.size()/sizeof(account);
        accs = new account[n];

        for (int i = 0; i<n; i++) {
            char buffer[sizeof(account)];
            in.readRawData(buffer, sizeof(account));
            memcpy(&accs[i], buffer, sizeof(account));
            ui->textEdit_2->append(QString("%1: R$%2").arg(accs[i].number).arg(QString::number(accs[i].balance)));
        }

        ui->label_11->setText(QString::number(n) + " contas encontradas");
    }
    accBin.close();
}

void MainWindow::on_pushButton_3_clicked()  //SELECIONAR ARQUIVO DE TRANSAÇÕES
{
    QString transactionsFile= QFileDialog::getOpenFileName(this, "Selecione o arquivo que contém as transações.", "", "Arquivos de Texto (*.txt)");
    transTxt.setFileName(transactionsFile);

    if( !transTxt.open(QIODevice::ReadWrite) ){
        ui->label_12->setText("Erro ao abrir arquivo");
    } else {
        txtLoaded = true;
        ui->textEdit_2->clear();
        transactions.clear();
        while(!transTxt.atEnd()){
            QString transaction = transTxt.readLine();
            transactions.append(transaction);
            int cents = (transaction.mid(6,10).remove(',')).toInt();
            ui->textEdit_2->append(transaction[5] + ": R$" + QString::number(cents).insert(QString::number(cents).length()-2, ',') + " em " + transaction.left(5));
        }
        ui->label_12->setText(QString::number(transactions.length()) + " transações encontradas.");
    }
    transTxt.close();
}

void MainWindow::on_pushButton_4_clicked()  //APLICA TRANSAÇÃO
{
    if (binLoaded && txtLoaded){
        int n = accBin.size()/sizeof(account);

        for (int i = 0; i < transactions.length(); ++i) {
            int cents = (transactions[i].mid(6,10).remove(',')).toInt();

            for (int j = 0;  j < n; j++) {
                if (QString::number(accs[j].number).toInt() == transactions[i].left(5).toInt()) { //APLICA TRANSAÇÕES

                    int trcents = (transactions[i].mid(6,10).remove(',')).toInt();
                    int currentcents = accs[j].balance*100;

                    if (transactions[i][5] == 'D') currentcents = currentcents + cents;
                    else if (transactions[i][5] == 'S') currentcents = currentcents - cents;

                    float final_value = roundf(currentcents) / 100.0f;
                    accs[j].balance = final_value;

                    QString textfinal = QString::number(final_value);
                }
            }
        }

        if( !accBin.open(QIODevice::ReadWrite) ){ //REESCREVE AS CONTAS
            ui->label_13->setText("Erro");
        } else {
            accBin.resize(0);
            for (int k = 0;  k < n; k++) {
                char buffer[sizeof(account)];
                memcpy(buffer, &accs[k], sizeof(account));

                accBin.seek(accBin.size());
                QDataStream out(&accBin);

                out.writeRawData(buffer, sizeof(account));
            }
            accBin.close();
            ui->label_13->setText("Contas Atualizadas");
        }

        ui->textEdit_2->setText("Novos saldos:");
        for (int i = 0; i<n; i++) { //
            ui->textEdit_2->append(QString("%1: R$%2").arg(accs[i].number).arg(QString::number(accs[i].balance)));
        }

    }
    else {
        ui->label_13->setText("Erro em arquivo(s)");
    }


}

void MainWindow::on_pushButton_5_clicked() //BUSCA TRANSAÇÕES DE CONTA
{
    ui->textEdit->clear();
    int n = accBin.size()/sizeof(account);

    for (int j = 0;  j < n; j++) { //ENCONTRA A CONTA
        if (QString::number(accs[j].number).toInt() == ui->lineEdit_6->text().toInt()) { //APLICA TRANSAÇÕES
            ui->textEdit->append("Transações de " + QString::fromUtf8(accs[j].name));
        }
    }

    for (int i = 0; i < transactions.length(); ++i) {

        if (ui->lineEdit_6->text().toInt() == transactions[i].left(5).toInt()) {
            QString operation;
            if (transactions[i][5] == 'D') operation = "Depósito";
            else if (transactions[i][5] == 'S') operation = "Saque";

            int trcents = (transactions[i].mid(6,10).remove(',')).toInt();
            QString txtval = QString::number(trcents);
            txtval.insert(txtval.length()-2, ',');
            ui->textEdit->append(operation + " de R$" + txtval);
        }
    }
}

void MainWindow::on_lineEdit_3_textChanged(const QString &arg1)  //MASCARA DE DINHEIRO
{
    QString value = arg1;
    value.remove(',');
    value.insert(value.length()-2, ",");
    ui->lineEdit_3->setText(value);
}
