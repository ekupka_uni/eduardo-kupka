#include "minimo.h"
#include <algorithm>
#include <QTextStream>

Minimo::Minimo(float numbers[10]){
    std::copy(numbers, numbers+10, list_of_numbers);
    QTextStream(stdout) << "Minimo inicializado" << endl;

}


float Minimo::calcula(){
    float min = list_of_numbers[0];
    for (int i = 1; i < 10; ++i) {
        if (list_of_numbers[i] < min) min = list_of_numbers[i];
    }
    return min;
}
