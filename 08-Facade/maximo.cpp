#include "maximo.h"
#include <algorithm>
#include <QTextStream>

Maximo::Maximo(float numbers[10]){
    std::copy(numbers, numbers+10, list_of_numbers);
    QTextStream(stdout) << "Maximo inicializado" << endl;

}


float Maximo::calcula(){
    float max = list_of_numbers[0];
    for (int i = 1; i < 10; ++i) {
        if (list_of_numbers[i] > max) max = list_of_numbers[i];
    }
    return max;
}
