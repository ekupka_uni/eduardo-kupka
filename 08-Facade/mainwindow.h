#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    float list_of_numbers[10] = {99.7801225, 67.83381189, 37.05062922, 26.96257487, 57.29350492, 37.10808201, 59.06136462, 85.97893817, 43.1520837, 3.22876843};

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
