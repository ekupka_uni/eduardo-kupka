#include "media.h"
#include <algorithm>
#include <QTextStream>

Media::Media(float numbers[10]){
    std::copy(numbers, numbers+10, list_of_numbers);
    QTextStream(stdout) << "Media inicializada" << endl;

}


float Media::calcula_media(){
    float media = 0;
    for (int i = 0; i < 10; ++i) {
        media = media + list_of_numbers[i];
    }
    return media/10;
}
