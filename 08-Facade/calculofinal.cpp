#include "QString"
#include <QTextStream>
#include "calculofinal.h"
#include "media.h"
#include "minimo.h"
#include "maximo.h"

CalculoFinal::CalculoFinal(float numbers[10]){
    std::copy(numbers, numbers+10, list_of_numbers);
    QTextStream(stdout) << "Calculo Total inicializado" << endl;
}

float CalculoFinal::calcular(){
    Media *media = new Media(list_of_numbers);
    Minimo *minimo = new Minimo(list_of_numbers);
    Maximo *maximo = new Maximo(list_of_numbers);

    results[0] = maximo->calcula();
    results[1] = media->calcula_media();
    results[2] = minimo->calcula();


    QTextStream(stdout) << QString("Max: " + QString::number(results[0]) + " | Med: " + QString::number(results[1]) + " | Min: " + QString::number(results[2])) << endl;

}
