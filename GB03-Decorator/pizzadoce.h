#ifndef PIZZADOCE_H
#define PIZZADOCE_H
#include "pizza.h"
#include "pizza.h"


class PizzaDoce : public Pizza
{
public:
    PizzaDoce();
    int getKcal();
    double getPrice();
    QString getDescription();
};

#endif // PIZZADOCE_H
