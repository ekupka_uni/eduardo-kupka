#ifndef QUEIJODECORATOR_H
#define QUEIJODECORATOR_H
#include "pizzadecorator.h"

class queijoDecorator : public pizzaDecorator
{
public:
    queijoDecorator(Pizza *decoratedPizza):
        pizzaDecorator(decoratedPizza){}

    int getKcal()
    {
        return m_decoratedPizza->getKcal() + 100;
    }

    double getPrice()
    {
        return m_decoratedPizza->getPrice() + 5;
    }

    QString getDescription(){
        return m_decoratedPizza->getDescription().chopped(1) + " com adicional de queijo.";
    }

};

#endif // QUEIJODECORATOR_H
