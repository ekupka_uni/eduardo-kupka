#include "pizzadoce.h"
#include <QTextStream>

PizzaDoce::PizzaDoce()
{

}

int PizzaDoce::getKcal()
{
    return 150;
}

double PizzaDoce::getPrice()
{
    return 20;
}

QString PizzaDoce::getDescription(){
    return QString("Pizza base doce.");
}
