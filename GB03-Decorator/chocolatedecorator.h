#ifndef CHOCOLATEDECORATOR_H
#define CHOCOLATEDECORATOR_H
#include "pizzadecorator.h"

class chocolateDecorator : public pizzaDecorator
{
public:
    chocolateDecorator(Pizza *decoratedPizza):
        pizzaDecorator(decoratedPizza){}

    int getKcal()
    {
        return m_decoratedPizza->getKcal() + 150;
    }

    double getPrice()
    {
        return m_decoratedPizza->getPrice() + 9;
    }

    QString getDescription(){
        return m_decoratedPizza->getDescription().chopped(1) + " com adicional de chocolate.";
    }
};

#endif // CHOCOLATEDECORATOR_H
