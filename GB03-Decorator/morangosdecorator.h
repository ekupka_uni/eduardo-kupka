#ifndef MORANGOSDECORATOR_H
#define MORANGOSDECORATOR_H
#include "pizzadecorator.h"

class morangosDecorator : public pizzaDecorator
{
public:
    morangosDecorator(Pizza *decoratedPizza):
        pizzaDecorator(decoratedPizza){}

    int getKcal()
    {
        return m_decoratedPizza->getKcal() + 30;
    }

    double getPrice()
    {
        return m_decoratedPizza->getPrice() + 5;
    }

    QString getDescription(){
        return m_decoratedPizza->getDescription().chopped(1) + " com adicional de morangos.";
    }
};

#endif // MORANGOSDECORATOR_H
