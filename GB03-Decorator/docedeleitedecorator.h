#ifndef DOCEDELEITEDECORATOR_H
#define DOCEDELEITEDECORATOR_H
#include "pizzadecorator.h"

class docedeleiteDecorator : public pizzaDecorator
{
public:
    docedeleiteDecorator(Pizza *decoratedPizza):
        pizzaDecorator(decoratedPizza){}

    int getKcal()
    {
        return m_decoratedPizza->getKcal() + 190;
    }

    double getPrice()
    {
        return m_decoratedPizza->getPrice() + 8;
    }

    QString getDescription(){
        return m_decoratedPizza->getDescription().chopped(1) + " com adicional de doce de leite.";
    }

};

#endif // DOCEDELEITEDECORATOR_H
