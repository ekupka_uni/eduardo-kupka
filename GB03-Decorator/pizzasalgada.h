#ifndef PIZZASALGADA_H
#define PIZZASALGADA_H
#include "pizza.h"

class PizzaSalgada : public Pizza
{
public:
    PizzaSalgada();
    int getKcal();
    double getPrice();
    QString getDescription();
};

#endif // PIZZASALGADA_H
