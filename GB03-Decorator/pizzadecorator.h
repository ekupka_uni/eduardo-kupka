#ifndef PIZZADECORATOR_H
#define PIZZADECORATOR_H
#import "pizza.h"

class pizzaDecorator : public Pizza
{
protected:
    Pizza *m_decoratedPizza;
public:
    pizzaDecorator(Pizza *decoratedPizza){
        m_decoratedPizza = decoratedPizza;
    };
};

#endif // PIZZADECORATOR_H
