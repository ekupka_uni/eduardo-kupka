#ifndef CONFEITOSDECORATOR_H
#define CONFEITOSDECORATOR_H
#include "pizzadecorator.h"

class confeitosDecorator : public pizzaDecorator
{
public:
    confeitosDecorator(Pizza *decoratedPizza):
        pizzaDecorator(decoratedPizza){}

    int getKcal()
    {
        return m_decoratedPizza->getKcal() + 20;
    }

    double getPrice()
    {
        return m_decoratedPizza->getPrice() + 2;
    }

    QString getDescription(){
        return m_decoratedPizza->getDescription().chopped(1) + " com adicional de confeitos.";
    }
};

#endif // CONFEITOSDECORATOR_H
