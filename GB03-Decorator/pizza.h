#ifndef PIZZA_H
#define PIZZA_H
#include <QString>

class Pizza
{
public:
    virtual int getKcal() = 0;
    virtual double getPrice() = 0;
    virtual QString getDescription() = 0;
    virtual ~Pizza(){}
};

#endif // PIZZA_H
