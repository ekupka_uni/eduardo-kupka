#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "pizza.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    void updateFields();
    Pizza *mainPizza = NULL;
    ~MainWindow();

private slots:
    void on_newPizzaSalgada_clicked();

    void on_newPizzaDoce_clicked();

    void on_addQueijo_clicked();

    void on_addTomate_clicked();

    void on_addCalabresa_clicked();

    void on_addCebola_clicked();

    void on_addPimentao_clicked();

    void on_addDoceDeLeite_clicked();

    void on_addMorangos_clicked();

    void on_addChocolate_clicked();

    void on_addConfeitos_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
