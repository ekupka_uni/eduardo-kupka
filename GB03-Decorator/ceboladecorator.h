#ifndef CEBOLADECORATOR_H
#define CEBOLADECORATOR_H
#include "pizzadecorator.h"

class cebolaDecorator : public pizzaDecorator
{
public:
    cebolaDecorator(Pizza *decoratedPizza):
        pizzaDecorator(decoratedPizza){}

    int getKcal()
    {
        return m_decoratedPizza->getKcal() + 5;
    }

    double getPrice()
    {
        return m_decoratedPizza->getPrice() + 1;
    }

    QString getDescription(){
        return m_decoratedPizza->getDescription().chopped(1) + " com adicional de cebola.";
    }
};

#endif // CEBOLADECORATOR_H
