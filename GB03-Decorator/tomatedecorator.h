#ifndef TOMATEDECORATOR_H
#define TOMATEDECORATOR_H
#include "pizzadecorator.h"

class tomateDecorator : public pizzaDecorator
{
public:
    tomateDecorator(Pizza *decoratedPizza):
        pizzaDecorator(decoratedPizza){}

    int getKcal()
    {
        return m_decoratedPizza->getKcal() + 60;
    }

    double getPrice()
    {
        return m_decoratedPizza->getPrice() + 3;
    }
    QString getDescription(){
        return m_decoratedPizza->getDescription().chopped(1) + " com adicional de tomate.";
    }

};

#endif // TOMATEDECORATOR_H
