#ifndef PIMENTAODECORATOR_H
#define PIMENTAODECORATOR_H
#include "pizzadecorator.h"

class pimentaoDecorator : public pizzaDecorator
{
public:
    pimentaoDecorator(Pizza *decoratedPizza):
        pizzaDecorator(decoratedPizza){}

    int getKcal()
    {
        return m_decoratedPizza->getKcal() + 10;
    }

    double getPrice()
    {
        return m_decoratedPizza->getPrice() + 2;
    }

    QString getDescription(){
        return m_decoratedPizza->getDescription().chopped(1) + " com adicional de pimentão.";
    }
};

#endif // PIMENTAODECORATOR_H
