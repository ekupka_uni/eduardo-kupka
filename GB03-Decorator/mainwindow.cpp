#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "pizza.h"
#include "pizzasalgada.h"
#include "pizzadoce.h"
#include "queijodecorator.h"
#include "tomatedecorator.h"
#include "calabresadecorator.h"
#include "ceboladecorator.h"
#include "pimentaodecorator.h"
#include "docedeleitedecorator.h"
#include "morangosdecorator.h"
#include "chocolatedecorator.h"
#include "confeitosdecorator.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_newPizzaSalgada_clicked()
{
    mainPizza =  new PizzaSalgada();
    updateFields();
}

void MainWindow::on_newPizzaDoce_clicked()
{
    mainPizza = new PizzaDoce();
    updateFields();
}

void MainWindow::updateFields(){
    ui->priceField->setText("R$ " + QString::number(mainPizza->getPrice()) + ",00");
    ui->kcalField->setText(QString::number(mainPizza->getKcal()) + " Kcal");
    ui->descriptionArea->clear();
    ui->descriptionArea->setText(mainPizza->getDescription());
}

void MainWindow::on_addQueijo_clicked()
{
    if (mainPizza){
        Pizza *queijo = new queijoDecorator(mainPizza);
        mainPizza = queijo;
        updateFields();
    }
}

void MainWindow::on_addTomate_clicked()
{
    if (mainPizza){
    Pizza *tomate = new tomateDecorator(mainPizza);
    mainPizza = tomate;
    updateFields();
    }
}

void MainWindow::on_addCalabresa_clicked()
{
    if (mainPizza){
    Pizza *calabresa = new calabresaDecorator(mainPizza);
    mainPizza = calabresa;
    updateFields();
    }
}

void MainWindow::on_addCebola_clicked()
{
    if (mainPizza){
    Pizza *cebola = new cebolaDecorator(mainPizza);
    mainPizza = cebola;
    updateFields();
    }
}

void MainWindow::on_addPimentao_clicked()
{
    if (mainPizza){
    Pizza *pimentao = new pimentaoDecorator(mainPizza);
    mainPizza = pimentao;
    updateFields();
    }
}

void MainWindow::on_addDoceDeLeite_clicked()
{
    if (mainPizza){
    Pizza *docedeleite = new docedeleiteDecorator(mainPizza);
    mainPizza = docedeleite;
    updateFields();
    }
}

void MainWindow::on_addMorangos_clicked()
{
    if (mainPizza){
        Pizza *morangos = new morangosDecorator(mainPizza);
        mainPizza = morangos;
        updateFields();
    }
}

void MainWindow::on_addChocolate_clicked()
{
    if (mainPizza){

        Pizza *chocolate = new chocolateDecorator(mainPizza);
        mainPizza = chocolate;
        updateFields();
    }
}

void MainWindow::on_addConfeitos_clicked()
{
    if (mainPizza){
        Pizza *confeitos = new confeitosDecorator(mainPizza);
        mainPizza = confeitos;
        updateFields();
    }
}
