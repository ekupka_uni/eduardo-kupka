#include "pizzasalgada.h"

PizzaSalgada::PizzaSalgada()
{

}

int PizzaSalgada::getKcal()
{
    return 120;
}

double PizzaSalgada::getPrice()
{
    return 20;
}

QString PizzaSalgada::getDescription(){
    return QString("Pizza base salgada.");
}
