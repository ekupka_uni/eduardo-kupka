#ifndef CALABRESADECORATOR_H
#define CALABRESADECORATOR_H
#include "pizzadecorator.h"

class calabresaDecorator : public pizzaDecorator
{
public:
    calabresaDecorator(Pizza *decoratedPizza):
        pizzaDecorator(decoratedPizza){}

    int getKcal()
    {
        return m_decoratedPizza->getKcal() + 80;
    }

    double getPrice()
    {
        return m_decoratedPizza->getPrice() + 6;
    }

    QString getDescription(){
        return m_decoratedPizza->getDescription().chopped(1) + " com adicional de calabresa.";
    }
};

#endif // CALABRESADECORATOR_H
