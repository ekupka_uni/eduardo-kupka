#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QTextStream>
#include <QString>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_pushButton_clicked()
{

   f.setFileName(ui->lineEdit->text());// Associa o objeto f a um arquivo chamado com nome digitado no campo

   if( !f.exists() ) {// Arquivo não existe
       ui->textEdit->setText("Arquivo não Existe.");
       return;
   }

   if( !f.open(QIODevice::ReadWrite | QIODevice::Text) ){ // Se existir abre
        ui->textEdit->setText( "Sem permissão para Abrir o arquivo.");
        return;
   }

   // Arquivo foi aberto. Agora necessita fechá-lo
   ui->textEdit->setText(f.readAll());


}

void MainWindow::on_pushButton_2_clicked()
{
    f.resize(0);
    QTextStream ts(&f);
    ts << ui->textEdit->toPlainText();
    f.close();
    return;
}
