#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    text = ui->textEdit->toPlainText();
    search = ui->lineEdit->text();

    if (caseSensitive){
        text.replace(search, QString("<font color=\"#FF0000\">" + search + "</font>"), Qt::CaseSensitive);
        timesFound = text.count(search, Qt::CaseSensitive);
    } else {
        text.replace(search, QString("<font color=\"#FF0000\">" + search + "</font>"), Qt::CaseInsensitive);
        timesFound = text.count(search, Qt::CaseInsensitive);
    }


    ui->textEdit->clear();
    ui->textEdit->append(text);

    ui->label_3->setText(QString("Found " + QString::number(timesFound) + " times."));

}

void MainWindow::on_checkBox_stateChanged(int arg1)
{
    if (arg1 == 2) caseSensitive = true;
    else caseSensitive = false;
}



void MainWindow::on_pushButton_2_clicked()
{
    ui->textEdit->clear();
    ui->lineEdit->clear();
    ui->label_3->clear();

}
